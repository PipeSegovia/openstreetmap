<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OpenStreet Maps</title>
    <!--Estilos en Home-->
    <link rel="stylesheet" href="css/estilos.css">

    <!--Libreria Leaflet-->
    <script src="Leaflet/docs/examples/libs/leaflet-src.js"></script>
    <link rel="stylesheet" href="Leaflet/docs/examples/libs/leaflet.css">
    <script src="Leaflet/src/Leaflet.draw.js"></script>
    <script src="Leaflet/src/Leaflet.Draw.Event.js"></script>
    <link rel="stylesheet" href="Leaflet/src/leaflet.draw.css">
    <script src="Leaflet/src/Toolbar.js"></script>
    <script src="Leaflet/src/Tooltip.js"></script>
    <script src="Leaflet/src/ext/GeometryUtil.js"></script>
    <script src="Leaflet/src/ext/LatLngUtil.js"></script>
    <script src="Leaflet/src/ext/LineUtil.Intersect.js"></script>
    <script src="Leaflet/src/ext/Polygon.Intersect.js"></script>
    <script src="Leaflet/src/ext/Polyline.Intersect.js"></script>
    <script src="Leaflet/src/ext/TouchEvents.js"></script>
    <script src="Leaflet/src/draw/DrawToolbar.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.Feature.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.SimpleShape.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.Polyline.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.Marker.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.Circle.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.CircleMarker.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.Polygon.js"></script>
    <script src="Leaflet/src/draw/handler/Draw.Rectangle.js"></script>
    <script src="Leaflet/src/edit/EditToolbar.js"></script>
    <script src="Leaflet/src/edit/handler/EditToolbar.Edit.js"></script>
    <script src="Leaflet/src/edit/handler/EditToolbar.Delete.js"></script>
    <script src="Leaflet/src/Control.Draw.js"></script>
    <script src="Leaflet/src/edit/handler/Edit.Poly.js"></script>
    <script src="Leaflet/src/edit/handler/Edit.SimpleShape.js"></script>
    <script src="Leaflet/src/edit/handler/Edit.Rectangle.js"></script>
    <script src="Leaflet/src/edit/handler/Edit.Marker.js"></script>
    <script src="Leaflet/src/edit/handler/Edit.CircleMarker.js"></script>
    <script src="Leaflet/src/edit/handler/Edit.Circle.js"></script>

    <!--Libreria JQuery-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>
<body>

    <h1>OpenStreet Map usando Leaflet</h1>
    
    <!--Mapa de OpenStreetMap-->
    <div id="map" class="osm"></div>

    <!--Formulario para determinar la longitud y latitud-->
    <section id="formulario" class="datos">
        <h2>Datos</h2>
        <p id="longitud"></p>
        <p id="latitud"></p>
    </section>
    <!--Formulario para peticion AJAX -->
    <div id="formJQuery"></div>


    <!--Formulario para la seleccion con Ajax-->
    <div class="form-seleccion">
        <button id="seleccion">Mostrar Areas</button>
    </div>
    <div id="form-seleccion"></div> <!--Formulario para peticion AJAX -->

    <!--Formulario para Actualizar con Ajax-->
    <div id="formUpdate"></div>

    <script src="js/mapa.js"></script>
    <!--Eventos para open street map-->
    <script src="js/validarBoton.js"></script>
    <!--Eventos para los botones-->
    <script src="js/eventosExternos.js"></script>

</body>
</html>