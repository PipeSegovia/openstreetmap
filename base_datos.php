<?php

class BaseDeDatos
{
    public $conexion;
    public $indice;

    public function __construct()
    {
        $this->conexion = mysqli_connect("localhost","root","","mapa");
        if(mysqli_connect_errno())
        {
            echo "<script> alert('La conexion a la bd ha fallado'); </script>";
        }

    }

    public function seleccionarIndice()
    {

        $query =  $this->conexion->query("SELECT indice FROM coordenadas");

        $valor = 0;
        
        while($row = $query->fetch_row())
        {
            $valor = $row;
        }


        if($valor == null)
        {
            //echo "<script> console.log('Valor nulo'); </script>";
            $indice = 1;
            return 1;
        }
        else
        {
            $indice = ($valor[count($valor) - 1] + 1);
            return $indice;
        }
        
    }

    public function insertarCoordenadas($longitudes, $latitudes)
    {

        $indice = $this->seleccionarIndice();

        for($i = 0; $i < count($longitudes); $i++)
        {
            $this->conexion->query("INSERT INTO coordenadas VALUES ($indice, $longitudes[$i], $latitudes[$i])");
        }
        //echo "<script> console.log('Datos insertados correctamente!'); </script>";
    }

    public function showAreas()
    {
        $tabla = array();

        $resultado = $this->conexion->query("SELECT * FROM coordenadas");
        if(mysqli_num_rows($resultado) > 0)
        {
            while($row = mysqli_fetch_assoc($resultado))
            {
                array_push($tabla,$row);
            }

        }
        
        mysqli_free_result($resultado);

        return $tabla;
    }

    public function actualizarArea($indice, $longLat)
    {
        
        //Borrar los datos de ese indice
        $borrar = $this->conexion->query("DELETE FROM coordenadas WHERE indice = $indice");
        //Insertar datos
        
        for($i = 0; $i < count($longLat); $i++)
        {
            //Aca quedeee
            echo '<br>Indice: '.$indice.' Longitud: '. $longLat[$i][1].' Latitud: '.$longLat[$i][0].'<br>';
            $this->conexion->query('INSERT INTO coordenadas VALUES ('.$indice.','. $longLat[$i][1].','. $longLat[$i][0].');');
        }
        
    }

    public function getIndice()
    {
        return $this->indice;
    }

}