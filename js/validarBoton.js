
var activarPoligono = false;
var editar = false;
var longitudes = [];
var latitudes = [];

//Evento para ver si se activo el boton polygon

map.on(L.Draw.Event.DRAWSTART, function (e) {
    var type = e.layerType,
        layer = e.layer;
    if (type === 'polygon') {
        activarPoligono = true;
    }

});

//Evento para ver si se desactivo el boton polygon

map.on(L.Draw.Event.DRAWSTOP, function (e) {
    var type = e.layerType,
        layer = e.layer;
    if (type === 'polygon') {
        activarPoligono = false;  
        
        //Vacio el arreglo
        longitudes.length = 0;
        latitudes.length = 0;

    }


});

//Evento que ocurre cuando el polygono ya esta creado
map.on(L.Draw.Event.CREATED, function (e){
    //Inserto datos
    $("#formJQuery").load("coordenadas.php",{long:longitudes,lat:latitudes});
    
});

//Obtengo los datos de longitud y latitud
map.on('click', function(e) {
    var longLabel = document.getElementById('longitud');
    var latLabel = document.getElementById('latitud');

    if(activarPoligono)
    {   
        //Imprimo Coordenadas
        longLabel.innerHTML = "Longitud: "+e.latlng.lng;
        latLabel.innerHTML = "Latitud: "+e.latlng.lat;

        longitudes.push(e.latlng.lng);
        latitudes.push(e.latlng.lat);
    }

});

//Evento cuando se actualizan los datos del poligono
map.on(L.Draw.Event.EDITED,function(e)
{
    var layers = e.layers;
    var latlngs;
    var coordenadas = [];
    var poligonos = [];

    layers.eachLayer(function (layer) {
        
        if (layer instanceof L.Polygon) {

            latlngs = layer.getLatLngs();

            for(var i = 0; i < latlngs.length; i++)
            {   
                for(var j = 0; j < latlngs[i].length; j++)
                {
                    coordenadas.push([latlngs[i][j].lat, latlngs[i][j].lng]);
                }
            }
        }

        poligonos.push(coordenadas);
        coordenadas = [];
    });


    $('#formUpdate').load('actualizar.php',{polygons:poligonos});

});