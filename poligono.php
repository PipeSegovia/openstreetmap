<?php

class Poligono
{

    public $indice;
    public $longitud;
    public $latitud;

    public function _construct()
    {
        $this->indice = 0;
        $this->longitud = array();
        $this->latitud = array();
    }

    public function ingresarLatLng($longitudes, $latitudes)
    {
        $this->indice++;
        $this->longitud = $longitudes;
        $this->latitud = $latitudes;
    }

    public function setIndice($nuevoIndice)
    {
        $this->indice = $nuevoIndice;
    }

    public function getIndice()
    {
        return $this->indice;
    }

    public function getLatitudes()
    {
        return $this->latitud;
    }

    public function getLongitudes()
    {
        return $this->longitud;
    }
}