<?php

    require('base_datos.php');
    require('poligono.php');

    session_start();
    

    $bd = new BaseDeDatos();
    $poligono = new Poligono();
    $longitud = array();
    $latitud = array();


    $tabla =  $bd->showAreas();
    for($j = 0; $j < $tabla[count($tabla)-1]['indice'];$j++)
    {
        for($i = 0; $i < count($tabla);$i++)
        {
            array_push($longitud,$tabla[$i]['longitud']);
            array_push($latitud,$tabla[$i]['latitud']);
        }
        $poligono->ingresarLatLng($longitud,$latitud);
    }
    
    $_SESSION['clasePoligono'] = $poligono;

    echo '<script> areas = '.json_encode($tabla).'</script>';